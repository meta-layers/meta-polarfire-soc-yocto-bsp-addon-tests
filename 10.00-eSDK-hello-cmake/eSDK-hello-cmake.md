# Objectives

Get/Build/Deploy `hellocmake`[1]

[1] https://gitlab.com/exempli-gratia/hellocmake.git

# Minimal build framework

@@@ see:

# Install the eSDK

@@@ see:

# `hellocmake` with the eSDK

Get into the SDK container:

```
[@buildhost:]
cd /workdir
./resy-sdk-container.sh
```

## SDK environment in SDK container

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/riscv/environment-setup-riscv64-resy-linux
```

You should see:

```
[@sdk-container:/ ]$
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## `devtool add`

```
[@sdk-container:/ ]$
devtool add hellocmake https://gitlab.com/exempli-gratia/hellocmake.git
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Starting bitbake server...
INFO: Fetching git://gitlab.com/exempli-gratia/hellocmake.git;protocol=https...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3674 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2360 .bb files complete (2356 cached, 4 parsed). 3678 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 0 Local 0 Network 0 Missed 0 Current 0 (0% match, 0% complete)
NOTE: No setscene tasks
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 2 tasks of which 0 didn't need to be rerun and all succeeded.
INFO: Using default source tree path /opt/resy/riscv/workspace/sources/hellocmake
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
NOTE: Reconnecting to bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
NOTE: Retrying server connection (#1)...
NOTE: Starting bitbake server...
INFO: Recipe /opt/resy/riscv/workspace/recipes/hellocmake/hellocmake_git.bb has been automatically created; further editing may be required to make it fully functional
[ sdk@28bc4b863813:/ ]$ 
```

## `devtool edit-recipe`

```
[@sdk-container:/ ]$
devtool edit-recipe hellocmake
```

You should see `vim` opening this `recipe`:

```
[@sdk-container:/ ]$
# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/hellocmake.git;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "d287d38ab4da45130d332ed332ab6a58fa92ba18"

S = "${WORKDIR}/git"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""
```

exit `vim` with `:q!`

## `devtool build`

```
[@sdk-container:/ ]$
devtool build hellocmake
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3674 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2360 .bb files complete (2356 cached, 4 parsed). 3678 targets, 359 skipped, 1 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3674 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2360 .bb files complete (2356 cached, 4 parsed). 3678 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:01
Sstate summary: Wanted 8 Local 1 Network 0 Missed 7 Current 124 (12% match, 94% complete)
NOTE: Executing Tasks
NOTE: hellocmake: compiling from external source tree /opt/resy/riscv/workspace/sources/hellocmake
NOTE: Tasks Summary: Attempted 568 tasks of which 559 didn't need to be rerun and all succeeded.
[ sdk@28bc4b863813:/ ]$ 
```

## `devtool deploy-target`


```
[@sdk-container:/ ]$
devtool deploy-target hellocmake root@hostname
```

You should see:

```
[@sdk-container:/ ]$
@@@ TODO
```

## run it on the target
```
ssh root@hostname
which hellocmake
hellocmake
```

(Optional) - build an image

## Build the Image

```
[@sdk-container:/ ]$
devtool build-image
```

## Flash the Image to the SD Card

### Set up the `eSDK` environment on the buildhost

I guess it would be a good idea to do this on the buildhost and not in the SDK container. 
You can use the `bmaptool`, which comes with the `eSDK`.

```
[@buildhost:]
source /opt/resy/riscv/environment-setup-riscv64-resy-linux
```

You should see something like this:

```
[@buildhost:]
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

### Flash the SD card on the buildhost

```
cd /opt/resy/riscv/tmp/deploy/images/m100pfsevp
sudo env "PATH=$PATH" bmaptool copy core-image-minimal-base-m100pfsevp.wic.xz /dev/<your SD card reader/writer>
```

## Boot it

And inspect the serial consoles of the board.