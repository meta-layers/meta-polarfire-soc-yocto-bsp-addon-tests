# Objectives

- Build 
  - u-boot
  - kernel
  - image 
- Flash image to SD card
- see it booting

# Minimal build framework

@@@ see:

# Install the eSDK

@@@ see:

# Build kernel

## SDK environment in SDK container

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/riscv/environment-setup-riscv64-resy-linux
```

You should see:

```
[@sdk-container:/ ]$
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## Get the kernel sources

```
[@sdk-container:/ ]$
time devtool modify virtual/kernel
```

Please note:
This takes a long (fetch) time under "normal" circumstances since it will download the sources.

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2357 cached, 2 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
INFO: Mapping virtual/kernel to mpfs-linux
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 2 Local 0 Network 0 Missed 2 Current 94 (0% match, 97% complete)
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 437 tasks of which 426 didn't need to be rerun and all succeeded.
INFO: Adding local source files to srctree...
INFO: Copying kernel config to srctree
INFO: Source tree extracted to /opt/resy/riscv/workspace/sources/mpfs-linux
INFO: Recipe mpfs-linux now set up to build from /opt/resy/riscv/workspace/sources/mpfs-linux

real    13m50.182s
user    0m2.028s
sys     0m1.184s
```

## Get the u-boot sources

Please note: 
This is a hack to build the SD card `wic` image! Under "normal" circumstances that's not needed.

```
[@sdk-container:/ ]$
time devtool modify u-boot
```

Please note, that this takes a long (fetch) time under "normal" circumstances since it will download the sources.

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:14
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
Removing 1 recipes from the m100pfsevp sysroot: 100% |#########################################################################################################################| Time: 0:00:00
INFO: SRC_URI contains some conditional appends/prepends - will create branches to represent these
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 0 Local 0 Network 0 Missed 0 Current 20 (0% match, 100% complete)
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 93 tasks of which 90 didn't need to be rerun and all succeeded.
INFO: Adding local source files to srctree...
INFO: Source tree extracted to /opt/resy/riscv/workspace/sources/u-boot
WARNING: SRC_URI is conditionally overridden in this recipe, thus several devtool-override-* branches have been created, one for each override that makes changes to SRC_URI. It is recommended that you make changes to the devtool branch first, then checkout and rebase each devtool-override-* branch and update any unique patches there (duplicates on those branches will be ignored by devtool finish/update-recipe)
INFO: Recipe u-boot now set up to build from /opt/resy/riscv/workspace/sources/u-boot

real    1m57.570s
user    0m1.328s
sys     0m0.244s
```

## Check status

```
[@sdk-container:/ ]$
devtool status
```

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
mpfs-linux: /opt/resy/riscv/workspace/sources/mpfs-linux
u-boot: /opt/resy/riscv/workspace/sources/u-boot
```

(Optional) - build an image

## Build the Image

```
[@sdk-container:/ ]$
time devtool build-image
```

This will take some (compile) time, since now we compile u-boot and kernel, plus the time it takes to install packages in the root file system.

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3674 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2359 .bb files complete (2355 cached, 4 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
WARNING: Skipping recipe mpfs-linux as it doesn't produce a package with the same name
INFO: Building image core-image-minimal with the following additional packages: u-boot
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:06
Loaded 3674 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:03
Parsing of 2359 .bb files complete (2354 cached, 5 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:06
Sstate summary: Wanted 81 Local 1 Network 0 Missed 80 Current 714 (1% match, 89% complete)
WARNING: The os-release:do_compile sig is computed to be 8f90dd61936754fe767a43ba646f94039bbf4f55438a745418e3db8abac3c1e0, but the sig is locked to 3deb050d0e0e46ceb79b3c15f1029d638674884be4faf90a25c20bd1da8f1aaf in SIGGEN_LOCKEDSIGS_t-allarch
The base-files:do_install sig is computed to be 5e714838e19a96cb0d73355e3e2676ae6650d16faecee83f7bf1b94416fbf139, but the sig is locked to b7b14c07ce375636ed2ed7e571cfa545c1f26d732d64f567292e08a7447c453d in SIGGEN_LOCKEDSIGS_t-m100pfsevp
Removing 3 stale sstate objects for arch riscv64: 100% |#######################################################################################################################| Time: 0:00:00
Removing 3 stale sstate objects for arch m100pfsevp: 100% |####################################################################################################################| Time: 0:00:00
NOTE: Executing Tasks
NOTE: u-boot: compiling from external source tree /opt/resy/riscv/workspace/sources/u-boot
NOTE: mpfs-linux: compiling from external source tree /opt/resy/riscv/workspace/sources/mpfs-linux
NOTE: Tasks Summary: Attempted 2232 tasks of which 1982 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Successfully built core-image-minimal. You can find output files in /opt/resy/riscv/tmp/deploy/images/m100pfsevp

real    4m42.039s
user    0m13.536s
sys     0m3.236s
```

## Re-Build the Image

Now that we already prebuilt kernel/u-boot it should be faster:

```
[@sdk-container:/ ]$
time devtool build-image
```

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3673 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2359 .bb files complete (2354 cached, 5 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
WARNING: Skipping recipe mpfs-linux as it doesn't produce a package with the same name
INFO: Building image core-image-minimal with the following additional packages: u-boot
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3673 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:03
Parsing of 2359 .bb files complete (2354 cached, 5 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:05
Sstate summary: Wanted 71 Local 1 Network 0 Missed 70 Current 724 (1% match, 91% complete)
WARNING: The os-release:do_compile sig is computed to be 8f90dd61936754fe767a43ba646f94039bbf4f55438a745418e3db8abac3c1e0, but the sig is locked to 3deb050d0e0e46ceb79b3c15f1029d638674884be4faf90a25c20bd1da8f1aaf in SIGGEN_LOCKEDSIGS_t-allarch
The base-files:do_install sig is computed to be 5e714838e19a96cb0d73355e3e2676ae6650d16faecee83f7bf1b94416fbf139, but the sig is locked to b7b14c07ce375636ed2ed7e571cfa545c1f26d732d64f567292e08a7447c453d in SIGGEN_LOCKEDSIGS_t-m100pfsevp
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 2232 tasks of which 2232 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Successfully built core-image-minimal. You can find output files in /opt/resy/riscv/tmp/deploy/images/m100pfsevp

real    0m32.395s
user    0m11.572s
sys     0m2.552s
```

## Flash the Image to the SD Card

### Set up the `eSDK` environment on the buildhost

I guess it would be a good idea to do this on the buildhost and not in the SDK container. 
You can use the `bmaptool`, which comes with the `eSDK`.

```
[@buildhost:]
source /opt/resy/riscv/environment-setup-riscv64-resy-linux
```

You should see something like this:

```
[@buildhost:]
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

### Flash the SD card on the buildhost

```
cd /opt/resy/riscv/tmp/deploy/images/m100pfsevp
sudo env "PATH=$PATH" bmaptool copy core-image-minimal-base-m100pfsevp.wic.xz /dev/<your SD card reader/writer>
```

## Boot it

And inspect the serial consoles of the board.












<!-- 
# kernel config fragment with the eSDK

## (optional) Get the kernel sources (from poky build container)

### Get into the poky build container:

```
[@buildhost:]
cd /workdir
 ./resy-poky-container.sh 
```

You should see something like this:

```
[@buildhost:]
+ docker pull reliableembeddedsystems/poky-container:2021-06-09-master-local-gcc-9-gui-icecc-ub18
2021-06-09-master-local-gcc-9-gui-icecc-ub18: Pulling from reliableembeddedsystems/poky-container
Digest: sha256:308ecc14ab4ab0436991aadd946c7429cd323ae6a69bbaf5f0750f4d1efb2d35
Status: Image is up to date for reliableembeddedsystems/poky-container:2021-06-09-master-local-gcc-9-gui-icecc-ub18
docker.io/reliableembeddedsystems/poky-container:2021-06-09-master-local-gcc-9-gui-icecc-ub18
+ set +x
 -- interactive mode --
source /workdir/resy-cooker.sh in container
+ press <ENTER> to go on

+ docker run --name poky_container --rm -it --add-host mirror:192.168.42.1 --net=host -v /home/student/projects:/projects -v /opt:/nfs -v /workdir:/workdir -v /workdir:/workdir reliableembeddedsystems/poky-container:2021-06-09-master-local-gcc-9-gui-icecc-ub18 --workdir=/workdir
pokyuser@e450-10:/workdir$ 
```

### Setup the environment

```
[@poky-build-container:]
source /workdir/resy-cooker.sh  m100pfsevp-polarfire-resy-master
```
You should see something like this:

```
[@poky-build-container:]
/workdir /workdir/build /workdir
MACHINE or MACHINE-sw-variant: m100pfsevp-polarfire-resy-master
initial SITE_CONF=../../sources/meta-resy/template-common/site.conf.sample
TEMPLATECONF: ../meta-aries-polarfire-resy-collection/meta-polarfire-soc-yocto-bsp-addon/template-m100pfsevp-polarfire-resy-master/
source ../sources/poky-master/oe-init-build-env m100pfsevp-polarfire-resy-master

### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    core-image-minimal
    core-image-minimal-base

You can also run generated qemu images with a command like 'runqemu qemux86'
/workdir/build /workdir
$#: 1
pokyuser    68    21  0 10:07 pts/0    00:00:00 /bin/bash /workdir/killall_bitbake.sh
pokyuser    70    68  0 10:07 pts/0    00:00:00 grep bitbake
pokyuser    68    21  0 10:07 pts/0    00:00:00 /bin/bash /workdir/killall_bitbake.sh
pokyuser    75    68  0 10:07 pts/0    00:00:00 grep bitbake
rm -f /workdir/build/m100pfsevp-polarfire-resy-master/hashserve.sock
to ./resy-poky-container.sh:
 -- non-interactive mode -- 
 add the image you want to build to the command line ./resy-poky-container.sh <MACHINE> <image>
 -- interactive mode --
   enter it with - ./resy-poky-container.sh <no param> 
   bitbake <image>
   source /workdir/resy-cooker.sh <MACHINE>
pokyuser@e450-10:/workdir/build$ 
```

devtool modify virtual/kernel 
devtool menuconfig

creates a fragment

... but kernel does not build

since it can not find the aries dts, dtsi files







This somehow does not seem to work - builds something

### Find kernel source dir

```
[@poky-build-container:]
bitbake mpfs-linux -c devshell
```


```
[@poky-build-container/devshell:]
sh-4.4# pwd
/workdir/build/m100pfsevp-polarfire-resy-master/tmp/work-shared/m100pfsevp/kernel-source
sh-4.4# exit
```

### Bundle it up to be used with eSDK/devtool

#### directoy on the build host

```
[@buildhost:]
pushd /opt/resy
sudo mkdir riscv-src
sudo chown ${USER}:${USER} riscv-src 
popd
```

#### copy sources over there

```
[@buildhost:]
mkdir -p /opt/resy/riscv-src
rsync -avp /workdir/build/m100pfsevp-polarfire-resy-master/tmp/work-shared/m100pfsevp/kernel-source /opt/resy/riscv-src
```

Get into the SDK container:

```
[@buildhost:]
cd /workdir
./resy-sdk-container.sh
```

## SDK environment in SDK container

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/riscv/environment-setup-riscv64-resy-linux
```

You should see:

```
[@sdk-container:/ ]$
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## What is the name of the kernel recipe?

```
[@sdk-container:/ ]$
devtool search linux | grep "Linux kernel"
```

You should see:

```
[@sdk-container:/ ]$
linux-firmware        Firmware files for use with Linux kernel
mpfs-linux            Linux kernel
```

## `devtool modify -n recipe srctree`
```
[@sdk-container:/ ]$
devtool modify -n mpfs-linux /opt/resy/riscv-src/kernel-source
```

We reduced the time a lot, since kernel sources are not fetched, but already here.

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
Removing 1 recipes from the m100pfsevp sysroot: 100% |#########################################################################################################################| Time: 0:00:00
INFO: Recipe mpfs-linux now set up to build from /opt/resy/riscv-src/kernel-source
[ sdk@28bc4b863813:/ ]$ 
```

## Build kernel

```
[@sdk-container:/ ]$
devtool build mpfs-linux
```

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:03
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 1 Local 0 Network 0 Missed 1 Current 116 (0% match, 99% complete)
NOTE: Executing Tasks
NOTE: mpfs-linux: compiling from external source tree /opt/resy/riscv-src/kernel-source
NOTE: Tasks Summary: Attempted 552 tasks of which 525 didn't need to be rerun and all succeeded.
```

## Modify Kernel config

```
[@sdk-container:/ ]$
devtool menuconfig mpfs-linux
```

Menuconfig should open.
Add `posix message queues`

```
[@sdk-container:/ ]$
General setup
  [*] POSIX Message Queues

Exit
Save new configuration


NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:02
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
INFO: Launching menuconfig
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 1 Local 0 Network 0 Missed 1 Current 94 (0% match, 98% complete)
NOTE: Executing Tasks
Currently  1 running tasks (436 of 436)  99% |############################################################################################################################################## |
0: mpfs-linux-5.6.x+git999-r0 do_menuconfig - 0s (pid 27511)
NOTE: Tasks Summary: Attempted 436 tasks of which 435 didn't need to be rerun and all succeeded.
```

exit `vim` with `:q!`

## `devtool build`

```
[@sdk-container:/ ]$
devtool build hellocmake
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3674 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2360 .bb files complete (2356 cached, 4 parsed). 3678 targets, 359 skipped, 1 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3674 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2360 .bb files complete (2356 cached, 4 parsed). 3678 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:01
Sstate summary: Wanted 8 Local 1 Network 0 Missed 7 Current 124 (12% match, 94% complete)
NOTE: Executing Tasks
NOTE: hellocmake: compiling from external source tree /opt/resy/riscv/workspace/sources/hellocmake
NOTE: Tasks Summary: Attempted 568 tasks of which 559 didn't need to be rerun and all succeeded.
[ sdk@28bc4b863813:/ ]$ 
```

## `devtool deploy-target`


```
[@sdk-container:/ ]$
devtool deploy-target hellocmake root@hostname
```

You should see:

```
[@sdk-container:/ ]$
@@@ TODO
```

## run it on the target
```
ssh root@hostname
which hellocmake
hellocmake
``` 
-->

