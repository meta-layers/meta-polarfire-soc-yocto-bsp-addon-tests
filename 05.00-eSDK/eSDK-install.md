# Objectives

Install and test the extensible SDK.

# Minimal build framework

@@@ TODO: Setup a minimal build framework.

# Install the eSDK

on buildhost (once):

Create a dir where we will install the SDK to:

```
[@buildhost:]
pushd /opt/resy
sudo mkdir riscv
sudo chown ${USER}:${USER} riscv
popd
```

Get into the SDK container:

```
[@buildhost:]
cd /workdir
./resy-sdk-container.sh
```

Install the SDK (once).
In the container:


minimal:
```
[@sdk-container:/ ]$
cd /workdir/build/m100pfsevp-polarfire-resy-master-minimal/tmp/deploy/sdk
time ./resy-systemd-glibc-x86_64-core-image-minimal-riscv64-m100pfsevp-toolchain-ext-3.3+snapshot.sh
```

```
[@sdk-container:/ ]$
cd /workdir/build/m100pfsevp-polarfire-resy-master/tmp/deploy/sdk
time ./resy-systemd-glibc-x86_64-core-image-minimal-base-riscv64-m100pfsevp-toolchain-ext-3.3+snapshot.sh
```

you should see/enter something like this:

```
[@sdk-container:/ ]$
Resy systemd (Reliable Embedded Systems Reference Distro) Extensible SDK installer version 3.3+snapshot
=======================================================================================================
Enter target directory for SDK (default: ~/resy-systemd_sdk): /opt/resy/riscv
You are about to install the SDK to "/opt/resy/riscv". Proceed [Y/n]? Y
Extracting SDK.....................................done
Setting it up...
Extracting buildtools...
Preparing build system...
Loading cache: 100% |                                                                                                                                                         | ETA:  --:--:--
Loaded 0 entries from dependency cache.
NOTE: imported (nativesdk-nettle-3.7.3-r0,x86_64-nativesdk,efca4048c517d1e3d6196cb069d2e45fcfc76ef3ca6bb6cf8ba2ddfd3a2bf9b2,0)################################################ | ETA:  0:00:00
NOTE: imported (nativesdk-python3-git-3.1.17-r0,x86_64-nativesdk,1309fb8cc8255065c35f67ab66fac51a0dbd78c48a35dbd90bec87d73544fdd9,0)
NOTE: imported (util-linux-libuuid-2.37-r0,riscv64,52212be9f8d8d70f073096475936e2453ead0bbcda91685d352dd7dfb6524c4b,0)
NOTE: imported (nativesdk-gdbm-1.19-r0,x86_64-nativesdk,b8e986d266d8666cf7f8c6169749f74938e90ee0aec08e06d813eb56b3ac8fcb,0)
NOTE: imported (kbd-2.4.0-r0,riscv64,d50856bd2c77656de3b43cf3ee3c8ee156897e9cc33bb0b692bdc2e4cdcb06a4,0)
NOTE: imported (nativesdk-make-4.3-r0,x86_64-nativesdk,c20b3ce30e13ca8eea130ae48d000d5281c16995fb5eb9dc5e6430369a098ee7,0)
NOTE: imported (nativesdk-bzip2-1.0.8-r0,x86_64-nativesdk,8d9e0401905582dfa29c14b91abae82ff699658fb69d314e162702eab0f9342d,0)
NOTE: imported (ca-certificates-20210119-r0,all,57a76456edd5ce22103380e218f433a0d609def07cdaedb4ec3f609554f332ee,0)
NOTE: imported (nativesdk-bash-completion-2.11-r0,x86_64-nativesdk,ea49b2ff3b48e85e4bafd412f590d9587b4b1182770081470aa5364811f0bb96,0)
NOTE: imported (nativesdk-qemuwrapper-cross-1.0-r0,x86_64-nativesdk,7e4dea529187530de90cf322148277126edef2d47fa952f51a98630b8c659349,0)
NOTE: imported (nativesdk-libunistring-0.9.10-r0,x86_64-nativesdk,b0907c03cca1d258fe372ce04635f5111a31dcd97256fb36a0871378d7afe7e7,0)
NOTE: imported (nativesdk-expat-2.4.1-r0,x86_64-nativesdk,6c5b021ef8b8574a7726cac139fd5dac539b998b7a34c100f34aa5fb1bf28291,0)
NOTE: imported (nativesdk-ncurses-6.2-r0,x86_64-nativesdk,aa0890279d077e578091eb2edd29be4f4503ac472b67572363a2a71b11fd2b1b,0)
...
NOTE: imported (glibc-2.33-r0,riscv64,90a0f3a8cb62706b93ab92d709b4692d435ee76b8916243b357452496b86937e,0)
NOTE: imported (nativesdk-bash-5.1-r0,x86_64-nativesdk,92a99f0d0716fcb379705c3e5e40ca81ef219713c78a4406cb535f17097c0e3a,0)
NOTE: imported (libnl-1_3.5.0-r0,riscv64,2f0f7c75a8440759d8c10b3de379b8feb4dc56c09198be714c574fb1ab16a510,0)
NOTE: imported (update-rc.d-0.8-r0,all,5b0d5f367cc05dde584c1436a09c49fb00928cd51d7a1f214d80e36ad3b9436f,0)
NOTE: imported (pciutils-3.7.0-r0,riscv64,36161c32433525b48184784d6da128d33f8d23fd7ed20c1223a435d47323f65d,0)
NOTE: imported (systemd-1_248.3-r0,riscv64,e608998a87e3832de0b6f4ab5a271bbdbf0ce33544759d1cb8dc2becc48ff7e1,0)
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:01:31
Parsing of 2359 .bb files complete (0 cached, 2359 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
Importing from file /opt/resy/riscv/conf/prserv.inc succeeded!
Loading cache: 100% |                                                                                                                                                         | ETA:  --:--:--
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:01:05
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:02
Checking sstate mirror object availability: 100% |#############################################################################################################################| Time: 0:00:00
WARNING: The os-release:do_compile sig is computed to be 0f9b29da00aa733c5b4592e188288cc93a52e31658c7cf0cb491fabc238ff7ac, but the sig is locked to 837b91781d58754a1efbdda8988c97929bb88e5dd6ea21de52192924b65df253 in SIGGEN_LOCKEDSIGS_t-allarch
The base-files:do_install sig is computed to be 49a2b510adc1b7ff48eddd35aa30ca2bffaac51cc73bd2eda2d2496ff0dd5882, but the sig is locked to 429561f32b726e18338bed4365ca6981578d3718e0b1e840090d8eac7e6bc157 in SIGGEN_LOCKEDSIGS_t-m100pfsevp
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
done
SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /opt/resy/riscv/environment-setup-riscv64-resy-linux
[ sdk@146328dd9021:/workdir/build/m100pfsevp-polarfire-resy-master/tmp/deploy/sdk ]$ 
```

# Try out the eSDK

## SDK environment in SDK container

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/riscv/environment-setup-riscv64-resy-linux
```

You should see:

```
[@sdk-container:/ ]$
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## Get U-Boot

We need to a little hack here (until/if ever the u-boot/hss stuff will be sorted). 
We need to build u-boot manually, since otherwise some files (e.g. boot.scr.uimg) required to built the image are not available where they should be.

```
[@sdk-container:/ ]$
devtool modify u-boot
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2357 cached, 2 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
INFO: SRC_URI contains some conditional appends/prepends - will create branches to represent these
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 0 Local 0 Network 0 Missed 0 Current 20 (0% match, 100% complete)
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 93 tasks of which 90 didn't need to be rerun and all succeeded.
INFO: Adding local source files to srctree...
INFO: Source tree extracted to /opt/resy/riscv/workspace/sources/u-boot
WARNING: SRC_URI is conditionally overridden in this recipe, thus several devtool-override-* branches have been created, one for each override that makes changes to SRC_URI. It is recommended that you make changes to the devtool branch first, then checkout and rebase each devtool-override-* branch and update any unique patches there (duplicates on those branches will be ignored by devtool finish/update-recipe)
INFO: Recipe u-boot now set up to build from /opt/resy/riscv/workspace/sources/u-boot
[ sdk@146328dd9021:/workdir/build/m100pfsevp-polarfire-resy-master/tmp/deploy/sdk ]$ 
```

## Build U-Boot

```
[@sdk-container:/ ]$
devtool build u-boot
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:01
Sstate summary: Wanted 8 Local 1 Network 0 Missed 7 Current 126 (12% match, 94% complete)
NOTE: Executing Tasks
NOTE: u-boot: compiling from external source tree /opt/resy/riscv/workspace/sources/u-boot
NOTE: Tasks Summary: Attempted 582 tasks of which 568 didn't need to be rerun and all succeeded.
```

## Build the Image

```
[@sdk-container:/ ]$
devtool build-image
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
INFO: Building image core-image-minimal-base with the following additional packages: u-boot
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:06
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2355 cached, 4 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:06
Checking sstate mirror object availability: 100% |#############################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 121 Local 1 Network 0 Missed 120 Current 981 (0% match, 89% complete)
WARNING: The os-release:do_compile sig is computed to be 0f9b29da00aa733c5b4592e188288cc93a52e31658c7cf0cb491fabc238ff7ac, but the sig is locked to 837b91781d58754a1efbdda8988c97929bb88e5dd6ea21de52192924b65df253 in SIGGEN_LOCKEDSIGS_t-allarch
The base-files:do_install sig is computed to be 49a2b510adc1b7ff48eddd35aa30ca2bffaac51cc73bd2eda2d2496ff0dd5882, but the sig is locked to 429561f32b726e18338bed4365ca6981578d3718e0b1e840090d8eac7e6bc157 in SIGGEN_LOCKEDSIGS_t-m100pfsevp
Removing 3 stale sstate objects for arch riscv64: 100% |#######################################################################################################################| Time: 0:00:00
Removing 2 stale sstate objects for arch m100pfsevp: 100% |####################################################################################################################| Time: 0:00:00
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 2899 tasks of which 2870 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Successfully built core-image-minimal-base. You can find output files in /opt/resy/riscv/tmp/deploy/images/m100pfsevp
[ sdk@146328dd9021:/workdir/build/m100pfsevp-polarfire-resy-master/tmp/deploy/sdk ]$
```

## Flash the Image to the SD Card

### Set up the `eSDK` environment on the buildhost

I guess it would be a good idea to do this on the buildhost and not in the SDK container. 
You can use the `bmaptool`, which comes with the `eSDK`.

```
[@buildhost:]
source /opt/resy/riscv/environment-setup-riscv64-resy-linux
```

You should see something like this:

```
[@buildhost:]
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

### Flash the SD card on the buildhost

```
cd /opt/resy/riscv/tmp/deploy/images/m100pfsevp
sudo env "PATH=$PATH" bmaptool copy core-image-minimal-base-m100pfsevp.wic.xz /dev/<your SD card reader/writer>
```

## Boot it

And inspect the serial consoles of the board.