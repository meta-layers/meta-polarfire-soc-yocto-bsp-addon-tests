HERE=$(pwd)

source subrepos.sh

cd ..

for subrepo in "${subrepos[@]}"; do
    echo "-->"
    echo "+ git subrepo pull ${subrepo}"
    time git subrepo pull ${subrepo}
    if [ $? != 0 ]; then
       echo "${RED}fix the Error!${NORMAL}"
       exit
    fi

    echo "<--"
done

# if you want to update poky you need to 
# erase it and git-subrepos-init.sh

#for subrepogithub in "${subreposgithub[@]}"; do
#          echo "-->"
#          echo "+ git subrepo pull ${subrepogithub}"
#          time git subrepo pull ${subrepogithub}
#          if [ $? != 0 ]; then
#             echo "${RED}fix the Error!${NORMAL}"
#             exit
#          fi
#          echo "<--"
#done

cd ${HERE}
