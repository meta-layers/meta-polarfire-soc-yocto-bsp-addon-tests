Anton Kuzmin‎: (4) -- LEDs -- write to 0x400000a0 -- 8 LSBs are visible on the 8 LEDs connected to the board
physical address


They are initialized to 0

...

root@m100pfsevp:~# devmem2 0x400000a0 w 0x00000000
/dev/mem opened.
Memory mapped at address 0x3ff61c0000.
Read at address  0x400000A0 (0x3ff61c00a0): 0x00000055
Write at address 0x400000A0 (0x3ff61c00a0): 0x00000000, readback 0x00000000

root@m100pfsevp:~# devmem2 0x400000a0 w 0x000000ff
/dev/mem opened.
Memory mapped at address 0x3fd4e73000.
Read at address  0x400000A0 (0x3fd4e730a0): 0x00000000
Write at address 0x400000A0 (0x3fd4e730a0): 0x000000FF, readback 0x000000FF

root@m100pfsevp:~# devmem2 0x400000a0 w 0x00000055
/dev/mem opened.
Memory mapped at address 0x3ff2485000.
Read at address  0x400000A0 (0x3ff24850a0): 0x000000FF
Write at address 0x400000A0 (0x3ff24850a0): 0x00000055, readback 0x00000055
