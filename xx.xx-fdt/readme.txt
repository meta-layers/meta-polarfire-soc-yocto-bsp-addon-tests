sdk@46a61e418360:/opt/resy/riscv/tmp/deploy/images/m100pfsevp$ mkimage -l fitImage
FIT description: Kernel fitImage for Resy systemd (Reliable Embedded Systems Reference Distro)/5.6.x+git999/m100pfsevp
Created:         Mon Jun 28 19:44:10 2021
 Image 0 (kernel-1)
  Description:  Linux kernel
  Created:      Mon Jun 28 19:44:10 2021
  Type:         Kernel Image
  Compression:  gzip compressed
  Data Size:    4424704 Bytes = 4321.00 KiB = 4.22 MiB
  Architecture: RISC-V
  OS:           Linux
  Load Address: 0x80200000
  Entry Point:  0x80200000
  Hash algo:    sha256
  Hash value:   11f130fc000a9b26c19a50e4f665ba3d789205aa9e6c20d972d3283a14662a74
 Image 1 (fdt-aries_m100pfsevp-emmc.dtb)
  Description:  Flattened Device Tree blob
  Created:      Mon Jun 28 19:44:10 2021
  Type:         Flat Device Tree
  Compression:  uncompressed
  Data Size:    9515 Bytes = 9.29 KiB = 0.01 MiB
  Architecture: RISC-V
  Load Address: 0x82200000
  Hash algo:    sha256
  Hash value:   12c1a402ae5b42b0b195d2317067d08e83f7a944bc12948201ccb5c4c3c5522a
 Image 2 (fdt-aries_m100pfsevp-sdcard.dtb)
  Description:  Flattened Device Tree blob
  Created:      Mon Jun 28 19:44:10 2021
  Type:         Flat Device Tree
  Compression:  uncompressed
  Data Size:    9628 Bytes = 9.40 KiB = 0.01 MiB
  Architecture: RISC-V
  Load Address: 0x82200000
  Hash algo:    sha256
  Hash value:   bf7827be478a154ca1f4855cc9df5c6e2c50ded96adfd78c8ae1bd714315da24
 Default Configuration: 'conf-aries_m100pfsevp-emmc.dtb'
 Configuration 0 (conf-aries_m100pfsevp-emmc.dtb)
  Description:  1 Linux kernel, FDT blob
  Kernel:       kernel-1
  FDT:          fdt-aries_m100pfsevp-emmc.dtb
  Hash algo:    sha256
  Hash value:   unavailable
 Configuration 1 (conf-aries_m100pfsevp-sdcard.dtb)
  Description:  0 Linux kernel, FDT blob
  Kernel:       kernel-1
  FDT:          fdt-aries_m100pfsevp-sdcard.dtb
  Hash algo:    sha256
  Hash value:   unavailable
sdk@46a61e418360:/opt/resy/riscv/tmp/deploy/images/m100pfsevp$ 


----

root@m100pfsevp:~# cd /boot
root@m100pfsevp:/boot# ls -lah
total 4.3M
drwxr-xr-x  2 root root 4.0K Mar  9  2018 .
drwxr-xr-x 19 root root 4.0K Jun 30 11:22 ..
lrwxrwxrwx  1 root root   15 Mar  9  2018 fitImage -> fitImage-5.6.16
-rw-r--r--  1 root root 4.3M Mar  9  2018 fitImage-5.6.16
root@m100pfsevp:/boot# 



------


tmp/images/

tmp/deploy/images/m100pfsevp⟫ ls *.its
fitImage-its--5.6.x+git0+960a4cc3ec-r0.0-m100pfsevp-20210627194940.its


--------


also *.dtb files

--------


